import React, { useState, useEffect, useContext, useRef ,useMemo} from "react";
import { Container, Row, Col, Button } from "react-bootstrap";
import { modeContext } from "../context/modeContext";

const GameOfLife = () => {
  const { toggle } = useContext(modeContext);

  const rows = 20;
  const cols = 20;
  const cellSize = 25;

  const createEmptyGrid = useMemo(() => {
    return () => Array.from({ length: rows }, () => Array(cols).fill(false));
  }, [rows, cols]);


  const [grid, setGrid] = useState(createEmptyGrid);
  const [isPaused, setIsPaused] = useState(false);

  const canvasRef = useRef(null);

  const togglePause = () => {
    setIsPaused((prevState) => !prevState);
  };

  useEffect(() => {
    let interval;

    if (!isPaused) {
      interval = setInterval(() => {
        updateGrid();
        drawGrid();
      }, 100);
    }

    return () => clearInterval(interval);
  }, [isPaused, grid]);

  useEffect(() => {
    const handleCanvasClick = (event) => {
      const rect = canvasRef.current.getBoundingClientRect();
      const mouseX = event.clientX - rect.left;
      const mouseY = event.clientY - rect.top;

      const clickedRow = Math.floor(mouseY / cellSize);
      const clickedCol = Math.floor(mouseX / cellSize);

      const newGrid = [...grid];
      newGrid[clickedRow][clickedCol] = !newGrid[clickedRow][clickedCol];
      setGrid(newGrid);
    };

    if (canvasRef.current) {
      canvasRef.current.addEventListener("click", handleCanvasClick);
    }

    return () => {
      if (canvasRef.current) {
        canvasRef.current.removeEventListener("click", handleCanvasClick);
      }
    };
  }, [grid, cellSize]);

  const drawGrid = () => {
    const canvas = canvasRef.current;
    if (!canvas) return;

    const ctx = canvas.getContext("2d");
    ctx.clearRect(0, 0, canvas.width, canvas.height);

    for (let i = 0; i < rows; i++) {
      for (let j = 0; j < cols; j++) {
        ctx.beginPath();
        ctx.rect(j * cellSize, i * cellSize, cellSize, cellSize);
        ctx.strokeStyle = "#ddd";
        ctx.stroke();

        if (grid[i][j]) {
          ctx.fillStyle = "#87BCD4";
          ctx.fill();
        }
      }
    }
  };

  const updateGrid = () => {
    const newGrid = createEmptyGrid();

    for (let i = 0; i < rows; i++) {
      for (let j = 0; j < cols; j++) {
        const neighbors = countNeighbors(i, j);

        if (grid[i][j]) {
          newGrid[i][j] = neighbors === 2 || neighbors === 3;
        } else {
          newGrid[i][j] = neighbors === 3;
        }
      }
    }

    setGrid(newGrid);
  };

  const countNeighbors = (row, col) => {
    let count = 0;

    for (let i = -1; i <= 1; i++) {
      for (let j = -1; j <= 1; j++) {
        const neighborRow = row + i;
        const neighborCol = col + j;

        if (
          neighborRow >= 0 &&
          neighborRow < rows &&
          neighborCol >= 0 &&
          neighborCol < cols
        ) {
          if (!(i === 0 && j === 0) && grid[neighborRow][neighborCol]) {
            count++;
          }
        }
      }
    }

    return count;
  };

  const randomizeGrid = () => {
    const newGrid = createEmptyGrid();

    for (let i = 0; i < rows; i++) {
      for (let j = 0; j < cols; j++) {
        newGrid[i][j] = Math.random() < 0.5;
      }
    }

    setGrid(newGrid);
  };

  const clearGrid = () => {
    setGrid(createEmptyGrid());
  };

  return ( 
    <Container className="pb-2">
      <Row >
        <Col>
          <canvas
            ref={canvasRef}
            width={cols * cellSize}
            height={rows * cellSize}
            style={{
              border: "2px solid #495d89",
              display: "block",
              margin: "20px auto",
              // Align the canvas to the start
            }}
          ></canvas>
        </Col>
      </Row>
      <Row>
        <Col>
        <div className="d-flex flex-row justify-content-center">
         <Button
              onClick={randomizeGrid}
              className={
                toggle ? "btn-game-dark fw-bold" : "fw-bold game-btn-dark"
              }
              size="sm"
              
            >
              Randomize
            </Button>
            <Button
              onClick={clearGrid}
              className={`mx-2 ${
                toggle ? "btn-game-dark fw-bold" : "fw-bold game-btn-dark"
              }`}
              size="sm"
            >
              Clear
            </Button>
            <Button
              onClick={togglePause}
              className={
                toggle ? "btn-game-dark fw-bold" : "fw-bold game-btn-dark"
              }
              size="sm"
            >
              {isPaused ? "Resume" : "Pause"}
            </Button>
         </div>
        </Col>
      </Row>
    </Container>
  );
};
export default GameOfLife;