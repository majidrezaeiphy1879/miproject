import React, { useContext, useMemo } from "react";
import {
  Col,
  Container,
  Card,
  Row,
  Image,
  Button,
  ProgressBar,
} from "react-bootstrap";
import Navbarmenue from "../components/Navbar";
import Sidenav from "../components/Sidenav";
import { modeContext } from "../context/modeContext";
import { FaMoon } from "react-icons/fa";
import { FaRegSun } from "react-icons/fa6";
import { MdArrowRight } from "react-icons/md";
import avatar from "../images/photo_2023-11-09_15-30-43.jpg";
const skillItems = {
  lang: [
    "Bootstrap, Angular",
    "React, Vue, Laravel",
    "Stylus, Sass, Less",
    "Gulp, Webpack, Grunt",
    "Tweenmax, GSAP",
  ],
  frameworks: [
    "Make UI/UX Design",
    "Create Mobile App",
    "Site Optimization",
    "Custom Website",
    "Data Analysis",
  ],
};
const education = [
  {
    years: ["2021-2022", "2016-2020", "2012-2015"],
    universities: ["Cardiff University", "BASU", "PNU"],
    degrees: [
      "MSc Astrophysics",
      "Bachelor of Mathematics",
      "Bachelor of Physics",
    ],
  },
];

const experience = [
  {
    years: ["2015-2021", "2022-2023"],
    universities: ["Freelancer", "Alacrity Foundation", "Cyber Innovation Hub"],
    title: ["Teaching", "Web Developer", "CyberSecurity"],
  },
];

const personalData = [
  {
    Birthday: "01.07.1990",
    Age: 18,
    Address: "Ave 11, New York, USA",
    Email: "tokyo@gmail.com",
    Phone: "+77 022 177 05 05",
    Nationality: "USA",
    Study: "Univercity of Texas",
    Degree: "Master",
    Intereset: "Playing Football",
    Freelance: "Available",
  },
];
const testimonialsData = [
  "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Sequi assumenda maiores a, quidem voluptatum unde?",
  "Another testimonial goes here. Lorem ipsum dolor sit amet consectetur adipisicing elit.",
  "Yet another testimonial. Lorem ipsum dolor sit amet consectetur adipisicing elit. Soluta, maxime.",
  "Yet another testimonial. Lorem ipsum dolor sit amet consectetur adipisicing elit. Soluta, maxime.",
];
function About() {
  const user = personalData[0];
  const isSmallScreen = window.innerWidth <= 420;
  const { toggle, handleClick } = useContext(modeContext);
  const educationMap = education.map((item, index) => {
    return (
      <div key={index}>
        {item.years.map((year, yearIndex) => (
          <div key={yearIndex}>
            <div className="d-flex">
              <div
                className="dot-styles"
                style={{
                  height: "19px",
                  width: "19px",
                  backgroundColor: toggle ? "#fff" : "#000",
                  border: "1px solid #ccc",
                  borderRadius: "100%",
                  position: "relative",
                  bottom:
                    yearIndex === 0
                      ? "16rem"
                      : yearIndex === 1
                      ? isSmallScreen
                        ? "205px"
                        : "200px"
                      : "9em",
                  right: "8px",
                }}
              ></div>
              <span
                className={toggle ? "edu-exp-light" : "edu-exp-dark"}
                style={{
                  bottom:
                    yearIndex === 0
                      ? "265px"
                      : yearIndex === 1
                      ? isSmallScreen
                        ? "213px"
                        : "210px"
                      : "153px",
                }}
              >
                {year}
              </span>
              <div
                className="d-flex flex-column responsive-edu-exp"
                style={{
                  bottom:
                    yearIndex === 0
                      ? "265px"
                      : yearIndex === 1
                      ? isSmallScreen
                        ? "219px"
                        : "214px"
                      : "152px",
                }}
              >
                <h3
                  className="edu-exp-h3-tags"
                  style={{ color: toggle ? "#000" : "#fff" }}
                >
                  {item.universities[yearIndex]}
                </h3>
                <span
                  className="edu-exp-span-tags"
                  style={{ color: toggle ? "#767676" : "#bbb" }}
                >
                  {item.degrees[yearIndex]}
                </span>
              </div>
            </div>
          </div>
        ))}
      </div>
    );
  });
  const experienceMap = experience.map((item, index) => {
    const maxIndex = Math.max(
      item.years.length,
      item.universities.length,
      item.title.length
    );

    return (
      <div key={index}>
        {[...Array(maxIndex)].map((_, yearIndex) => {
          const year = item.years[Math.min(yearIndex, item.years.length - 1)];
          const university =
            item.universities[
              Math.min(yearIndex, item.universities.length - 1)
            ];
          const title = item.title[Math.min(yearIndex, item.title.length - 1)];

          return (
            <div key={yearIndex}>
              <div className="d-flex">
                <div
                  className="dot-styles"
                  style={{
                    height: "19px",
                    width: "19px",
                    backgroundColor: toggle ? "#fff" : "#000",
                    border: "1px solid #ccc",
                    borderRadius: "100%",
                    position: "relative",
                    bottom:
                      yearIndex === 0
                        ? "16rem"
                        : yearIndex === 1
                        ? isSmallScreen
                          ? "196px"
                          : "200px"
                        : "9em",
                    right: "8px",
                  }}
                ></div>
                <span
                  className={toggle ? "edu-exp-light" : "edu-exp-dark"}
                  style={{
                    bottom:
                      yearIndex === 0
                        ? "265px"
                        : yearIndex === 1
                        ? isSmallScreen
                          ? "205px"
                          : "210px"
                        : "153px",
                  }}
                >
                  {year}
                </span>
                <div
                  className="d-flex flex-column responsive-edu-exp"
                  style={{
                    bottom:
                      yearIndex === 0
                        ? "265px"
                        : yearIndex === 1
                        ? "214px"
                        : yearIndex === 2
                        ? "155px"
                        : "152px",
                  }}
                >
                  <h3
                    className="edu-exp-h3-tags"
                    style={{ color: toggle ? "#000" : "#fff" }}
                  >
                    {university}
                  </h3>
                  <span
                    className="edu-exp-span-tags"
                    style={{ color: toggle ? "#767676" : "#bbb" }}
                  >
                    {title}
                  </span>
                </div>
              </div>
            </div>
          );
        })}
      </div>
    );
  });

  const [langSkills, frameworkSkills] = useMemo(() => {
    const lang = skillItems.lang.map((item, index) => (
      <div key={index} className="d-flex flex-row">
        <MdArrowRight
          style={{
            height: "20px",
            width: "20px",
            color: toggle ? "#000" : "#fff",
          }}
        />
        <p
          className="knowledge-interest-items"
          style={{ color: toggle ? "#767676" : "#bbb" }}
        >
          {item}
        </p>
      </div>
    ));

    const frameworks = skillItems.frameworks.map((item, index) => (
      <div key={index} className="d-flex flex-row">
        <MdArrowRight
          style={{
            height: "20px",
            width: "20px",
            color: toggle ? "#000" : "#fff",
          }}
        />
        <p
          className="knowledge-interest-items"
          style={{ color: toggle ? "#767676" : "#bbb" }}
        >
          {item}
        </p>
      </div>
    ));

    return [lang, frameworks];
  }, [toggle]);
  const testimonialsMap = testimonialsData.map((testimonial, index) => (
    <>
      <div
        key={index}
        className="column-quote"
        style={{ border: toggle ? "1px solid #bbb" : "1px solid #efefef" }}
      >
        <p
          className="quote thigstodo-data px-5"
          style={{ color: toggle ? "#767676" : "#bbb" }}
        >
          {testimonial}
        </p>
      </div>
      <div className="d-flex justify-content-start align-items-center py-4 ms-5">
        <div
          className="slider-logo thigstodo-data "
          style={{ color: toggle ? "#000" : "#fff" }}
        >
          <h3 className=" ms-5 ps-2 about-bottom-slider" style={{ whiteSpace: "nowrap" }}>
            David Walker
          </h3>
        </div>
      </div>
    </>
  ));

  return (
    <Container fluid>
      <Row className="d-xl-none">
        <Col
          className={toggle ? "px-0 top-navbar-dark" : "px-0 top-navbar-light"}
        >
          <Navbarmenue />
        </Col>
      </Row>
      <Row>
        <Col
          xl={3}
          className={
            toggle
              ? " vh-100 fixed-col fixed-col-light "
              : " vh-100 fixed-col-dark  fixed-col"
          }
          style={{ zIndex: "10", position: "sticky", top: 0 }}
        >
          <Sidenav />
        </Col>
        <Col
          className={toggle ? " bg-color-dark pt-5" : " bg-color-light pt-5"}
          style={{ overflowY: "auto" }}
        >
          <span
            className={
              toggle
                ? "px-3 py-2 first-about-light first-part-marings"
                : "px-3 py-2 first-about-dark first-part-marings"
            }
          >
            ABOUT
          </span>
          <h3
            style={{ color: toggle ? "#000" : "#fff" }}
            className={
              toggle
                ? "mt-3 mb-5 about-senond-about first-part"
                : " about-senond-about mt-3 mb-5 first-part"
            }
          >
            About Me
          </h3>
          <Image
            src={avatar}
            style={{ width: "100%" }}
            className="first-part"
          />
          <h3
            className={
              toggle
                ? "mt-4 type-self-light fonts-general first-part"
                : " fonts-general mt-4 type-self-dark first-part"
            }
          >
            Adriano Smith
          </h3>
          <span
            className="web-dev-about first-part"
            style={{ color: toggle ? "#767676" : "#bbb" }}
          >
            Web Developer
          </span>
          <hr
            style={{ color: toggle ? "#767676" : "#bbb" }}
            className="mb-4 first-part"
          />
          <div>
            <p
              className="about-detail first-part"
              style={{ color: toggle ? "#767676" : "#bbb" }}
            >
              Hello, I'm Majid. A few years ago, I immersed myself in the world
              of mathematics and physics, earning an MSc in Astrophysics from
              Cardiff University and becoming a PhD candidate. During this time,
              my academic pursuits were centered around the complexities of
              physics, demonstrating my dedication to the subject. However, I
              decided to transition my career towards web development, with a
              specific focus on frontend development using React. With a strong
              foundation in Python and over three years of experience in
              JavaScript, I have actively contributed to various projects,
              emphasizing both web development and cybersecurity. This journey
              has allowed me to merge my analytical background with a newfound
              passion for crafting innovative and user-centric digital
              experiences.
            </p>
          </div>
          <hr style={{ color: toggle ? "#767676" : "#bbb" }} />

          {toggle ? (
            <div className="about-toggle-btn-dark " onClick={handleClick}>
              <FaMoon
                style={{
                  height: "35px",
                  width: "25px",
                  marginLeft: "10px",
                  marginTop: "7px",
                  cursor: "pointer",
                }}
              />
            </div>
          ) : (
            <div className="about-toggle-btn-light" onClick={handleClick}>
              <FaRegSun
                style={{
                  height: "30px",
                  width: "25px",
                  marginLeft: "1rem",
                  marginTop: "8px",
                  color: "white",
                  cursor: "pointer",
                }}
              />
            </div>
          )}
          <Row className="first-part">
            <Col md={6}>
              <div className="personal-data-column">
                {Object.entries(user)
                  .slice(0, Math.ceil(Object.keys(user).length / 2))
                  .map(([key, value]) => (
                    <div className="personal-data-item" key={key}>
                      <span className="personal-data-key" style={{color:toggle?"#000":"#fff"}}>{key}:</span>
                      <span className="personal-data-value thigstodo-data"  style={{color:toggle?"#767676":"#bbb"}}>{value}</span>
                    </div>
                  ))}
              </div>
            </Col>
            <Col md={6}>
              <div className="personal-data-column">
                {Object.entries(user)
                  .slice(Math.ceil(Object.keys(user).length / 2))
                  .map(([key, value]) => (
                    <div className="personal-data-item" key={key}>
                      <span className="personal-data-key"  style={{color:toggle?"#000":"#fff"}}>{key}:</span>
                      <span className="personal-data-value thigstodo-data" style={{color:toggle?"#767676":"#bbb"}}>{value}</span>
                    </div>
                  ))}
              </div>
            </Col>
          </Row>
          <hr
            className="mb-4 mt-4 first-part"
            style={{ color: toggle ? "#767676" : "#bbb" }}
          />

          <button
            className={
              toggle
                ? "cv-button-dark mb-5 mt-4 maring-for-all first-part-marings "
                : "cv-button-light mb-5 mt-4 maring-for-all first-part-marings"
            }
          >
            <a
              href="#"
              className={
                toggle
                  ? "text-decoration-none text-white "
                  : "text-decoration-none text-dark"
              }
            >
              Download CV
            </a>
          </button>
          <Row
            style={{
              backgroundColor: toggle ? "#fff" : "#000",
              width: "100%",
              marginRight: "0",
              paddingRight: "0",
            }}
          >
            <Col md={6} className="my-5 ">
              <div className="first-part">
                <h3
                  className="bar-progress-title pb-5"
                  style={{ color: toggle ? "#000" : "#fff" }}
                >
                  Programming Skills
                </h3>

                <div
                  className="d-flex justify-content-between bar-progress-items mb-2"
                  style={{ color: toggle ? "#000" : "#efefef" }}
                >
                  <span>WordPress</span>
                  <span>95%</span>
                </div>
                <ProgressBar
                  now={95}
                  style={{ height: "3px", backgroundColor: "#bbb" }}
                  variant="secondary"
                  className=" mb-3"
                />

                <div
                  className="d-flex justify-content-between bar-progress-items  mb-2"
                  style={{ color: toggle ? "#000" : "#efefef" }}
                >
                  <span>JavaScript</span>
                  <span>80%</span>
                </div>
                <ProgressBar
                  now={80}
                  style={{ height: "3px", backgroundColor: "#bbb" }}
                  variant="secondary"
                  className=" mb-3"
                />

                <div
                  className="d-flex justify-content-between bar-progress-items  mb-2"
                  style={{ color: toggle ? "#000" : "#efefef" }}
                >
                  <span>Angular</span>
                  <span>90%</span>
                </div>
                <ProgressBar
                  now={90}
                  style={{ height: "3px", backgroundColor: "#bbb" }}
                  variant="secondary"
                  className=" mb-3"
                />
              </div>
            </Col>
            <Col md={6} className="my-5">
              <div className="first-part">
                <h3
                  className="bar-progress-title pb-5"
                  style={{ color: toggle ? "#000" : "#fff" }}
                >
                  Language Skills
                </h3>
                <div
                  className="d-flex justify-content-between bar-progress-items  mb-2"
                  style={{ color: toggle ? "#000" : "#efefef" }}
                >
                  <span>English</span>
                  <span>95%</span>
                </div>
                <ProgressBar
                  now={95}
                  style={{ height: "3px", backgroundColor: "#bbb" }}
                  variant="secondary"
                  className=" mb-3"
                />

                <div
                  className="d-flex justify-content-between bar-progress-items  mb-2"
                  style={{ color: toggle ? "#000" : "#efefef" }}
                >
                  <span>Russian</span>
                  <span>80%</span>
                </div>
                <ProgressBar
                  now={80}
                  style={{ height: "3px", backgroundColor: "#bbb" }}
                  variant="secondary"
                  className=" mb-3"
                />

                <div
                  className="d-flex justify-content-between bar-progress-items  mb-2"
                  style={{ color: toggle ? "#000" : "#efefef" }}
                >
                  <span>Arabic</span>
                  <span>90%</span>
                </div>
                <ProgressBar
                  now={90}
                  style={{ height: "3px", backgroundColor: "#bbb" }}
                  variant="secondary"
                  className=" mb-3"
                />
              </div>
            </Col>
          </Row>
          <Row className="first-part">
            <Col md={6} className="my-5">
              <h3
                className="mb-4 bar-progress-title pb-5"
                style={{ color: toggle ? "#000" : "#fff" }}
              >
                Knowledge
              </h3>
              {langSkills}
            </Col>
            <Col md={6} className="my-5">
              <h3
                className="mb-4 bar-progress-title pb-5"
                style={{ color: toggle ? "#000" : "#fff" }}
              >
                Interests
              </h3>
              {frameworkSkills}
            </Col>
          </Row>
          <Row
            className="d-flex flex-column flex-md-row first-part"
            style={{ backgroundColor: toggle ? "#fff" : "#000" }}
          >
            <Col className="pt-5 ps-3">
              <h3
                className="pb-3 edu-exp-h3-tags"
                style={{ color: toggle ? "#000" : "#fff" }}
              >
                Education
              </h3>
              <div
                style={{ borderLeft: "1px solid #8da08d", height: "280px" }}
              ></div>
              {educationMap}
            </Col>
            <Col className="pt-5 ps-3">
              <h3
                className="pb-3 edu-exp-h3-tags"
                style={{ color: toggle ? "#000" : "#fff" }}
              >
                Experience
              </h3>
              <div
                style={{ borderLeft: "1px solid #8da08d", height: "280px" }}
              ></div>
              {experienceMap}
            </Col>
          </Row>
          <Row className="first-part py-5">
            <h3
              className="pb-3 edu-exp-h3-tags"
              style={{ color: toggle ? "#000" : "#fff" }}
            >
              Testimonials
            </h3>
            <div
              className="d-flex flex-rwo  testimonial-container"
              style={{ cursor: "e-resize" }}
            >
              {testimonialsMap.map((testimonial, index) => (
                <Col md={6} xs={12} sm={12} lg={6} xl={6} key={index}>
                  {testimonial}
                </Col>
              ))}
            </div>
          </Row>
        </Col>
      </Row>
    </Container>
  );
}

export default About;
