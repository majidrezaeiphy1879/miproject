import React from "react";
import { useContext, useState } from "react";
import { Container, Row, Col, Card, Image, Modal } from "react-bootstrap";
import { modeContext } from "../context/modeContext";
import Navbarmenue from "../components/Navbar";
import Sidenav from "../components/Sidenav";
import { FaMoon } from "react-icons/fa";
import { FaRegSun } from "react-icons/fa6";
const services = [
  {
    id: '01',
    title: "Web Design",
    data: "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Nisi perspiciatis facilis vitae similique molestias magnam.",
  },
  {
    id: '02',
    title: "Content Writing",
    data: "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Nisi perspiciatis facilis vitae similique molestias magnam.",
  },
  {
    id: '03',
    title: "Brand Identity",
    data: "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Nisi perspiciatis facilis vitae similique molestias magnam.",
  },
  {
    id: '04',
    title: "Live Chat",
    data: "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Nisi perspiciatis facilis vitae similique molestias magnam.",
  },
  {
    id: '05',
    title: "After Effects",
    data: "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Nisi perspiciatis facilis vitae similique molestias magnam.",
  },
  {
    id: '06',
    title: "Mobile App",
    data: "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Nisi perspiciatis facilis vitae similique molestias magnam.",
  },
];
const images = [
  "https://tokyo-nextjs.netlify.app/_ipx/w_256,q_75/%2Fimg%2Fpartners%2Fdark%2F1.png?url=%2Fimg%2Fpartners%2Fdark%2F1.png&w=256&q=75",
  "https://tokyo-nextjs.netlify.app/_ipx/w_256,q_75/%2Fimg%2Fpartners%2Fdark%2F2.png?url=%2Fimg%2Fpartners%2Fdark%2F2.png&w=256&q=75",
  "https://tokyo-nextjs.netlify.app/_ipx/w_256,q_75/%2Fimg%2Fpartners%2Fdark%2F3.png?url=%2Fimg%2Fpartners%2Fdark%2F3.png&w=256&q=75",
  "https://tokyo-nextjs.netlify.app/_ipx/w_256,q_75/%2Fimg%2Fpartners%2Fdark%2F5.png?url=%2Fimg%2Fpartners%2Fdark%2F5.png&w=256&q=75",
  "https://tokyo-nextjs.netlify.app/_ipx/w_256,q_75/%2Fimg%2Fpartners%2Fdark%2F1.png?url=%2Fimg%2Fpartners%2Fdark%2F1.png&w=256&q=75",
  "https://tokyo-nextjs.netlify.app/_ipx/w_256,q_75/%2Fimg%2Fpartners%2Fdark%2F2.png?url=%2Fimg%2Fpartners%2Fdark%2F2.png&w=256&q=75",
  "https://tokyo-nextjs.netlify.app/_ipx/w_256,q_75/%2Fimg%2Fpartners%2Fdark%2F3.png?url=%2Fimg%2Fpartners%2Fdark%2F3.png&w=256&q=75",
  "https://tokyo-nextjs.netlify.app/_ipx/w_256,q_75/%2Fimg%2Fpartners%2Fdark%2F5.png?url=%2Fimg%2Fpartners%2Fdark%2F5.png&w=256&q=75",
];
const FunFacs = [
  { nums: '777+', title: "Projects Complete" },
  { nums: "3K", title: "Happy Clients" },
  { nums: "9K+", title: "Line of Code" },
];

const pricing = [
  {
    price: "0",
    type: "Free",
    text: "Premium Icons Quality Logo Stock Images Free Support",
  },
  {
    price: "30",
    type: "Basic",
    text: "Premium Icons Quality Logo Stock Images Free Support",
  },
  {
    price: "70",
    type: "Premium",
    text: "Premium Icons Quality Logo Stock Images Free Support",
  },
];
function Services() {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const { toggle, handleClick } = useContext(modeContext);
  const thingsTodo = (
    <Row
      xs={1}
      lg={4}
      xl={4}
      className="mx-2 mb-5 d-flex justify-content-center "
    >
      {services.map((item, index) => (
        <Col
          key={index}
          className="sevices-items me-3 mb-4 "
          style={{ backgroundColor: toggle ? "#fff" : "#323232" }}
          onClick={handleShow}
        >
          <ul>
            <li className="d-flex flex-column">
              <span
                className="thigstodo-id"
                style={{
                  backgroundColor: toggle ? "#F4F7F8" : "#414546",
                  color: toggle ? "#000" : "#fff",
                }}
              >
                {item.id}
              </span>
              <span
                className="span-h3-service"
                style={{ color: toggle ? "#000" : "#fff" }}
              >
                {item.title}
              </span>
              <span style={{ color: toggle ? "#767676" : "#bbb" }} className="thigstodo-data">
                {item.data}
              </span>

              <div class="read-more ">
                <div class="dash" style={{ color: toggle ? "#000" : "#fff" }}>
                  ——
                </div>
                <div
                  class="text my-1"
                  style={{ color: toggle ? "#000" : "#fff" }}
                >
                  Read More
                </div>
              </div>
            </li>
          </ul>
        </Col>
      ))}
    </Row>
  );
  const partners = images.map((item, index) => (
    <Col key={index} md={4} lg={3} xs={4} style={{ border: "2px solid #eee"}} >
      <ul style={{ listStyleType: "none" }} className="p-0 ">
        <li style={{ height: "125px", width: "100%" }} className="d-flex justify-content-center align-items-center">
          <div >
            <img
              src={item}
              alt=""
              style={{ maxWidth: "100%", maxHeight: "100%", objectFit: "contain"}}
              className="servies-images"
            />
          </div>
        </li>
      </ul>
    </Col>
  ));
  
  

  const funFact = FunFacs.map((item, index) => (
    <Col key={index} md={4} lg={4} className="my-3">
      <ul
        style={{ border: "1px solid gray", listStyle: "none" }}
        className="p-0 "
      >
        <li className="text-center py-5">
          <h3
            className="fun-fact-h3"
            style={{ color: toggle ? "#000" : "#fff" }}
          >
            {item.nums}
          </h3>
          <span style={{ color: toggle ? "#767676" : "#bbb" }} className="thigstodo-data">
            {item.title}
          </span>
        </li>
      </ul>
    </Col>
  ));

  const priceTag = pricing.map((item, index) => (
    <Col key={index} md={4} lg={4} className="my-3">
      <ul style={{ border: "1px solid gray", listStyle: "none" }} >
        <li className="text-center py-5 p-0 d-flex flex-column justify-content-start align-items-start ">
          <span
            className="prince-price "
            style={{ color: toggle ? "#000" : "#fff" }}
          >
            {item.price}
          </span>
          <h3 style={{ color: toggle ? "#767676" : "#bbb", fontSize: "20px" }}>
            {item.type}
          </h3>
          <hr style={{ color: toggle ? "#767676" : "#bbb", width: "90%" }} />

          <p
            className="w-50 price-p"
            style={{ color: toggle ? "#767676" : "#bbb" }}
          >
            {item.text}
          </p>
        </li>
        <button
          className={toggle ? "cv-button-dark mb-4" : "cv-button-light mb-4"}
        >
          <a
            href="#"
            className={
              toggle
                ? "text-decoration-none text-white "
                : "text-decoration-none text-dark"
            }
          >
            Purchase
          </a>
        </button>
      </ul>
    </Col>
  ));

  return (
    <Container fluid>
      <Row className="d-xl-none">
        <Col className="px-0">
          <Navbarmenue />
        </Col>
      </Row>
      <Row>
        <Col
          xl={3}
          className={
            toggle
              ? " vh-100 fixed-col-light fixed-col"
              : " vh-100 fixed-col-dark  fixed-col"
          }
          style={{ zIndex: "10", position: "sticky", top: 0 }}
        >
          <Sidenav />
        </Col>
        <Col
          className={toggle ? "bg-color-dark  " : "bg-color-light "}
          style={{ overflowY: "auto" }}
        >
          {toggle ? (
            <div className="about-toggle-btn-dark " onClick={handleClick}>
              <FaMoon
                style={{
                  height: "35px",
                  width: "25px",
                  marginLeft: "10px",
                  marginTop: "7px",
                  cursor: "pointer",
                }}
              />
            </div>
          ) : (
            <div className="about-toggle-btn-light" onClick={handleClick}>
              <FaRegSun
                style={{
                  height: "30px",
                  width: "25px",
                  marginLeft: "1rem",
                  marginTop: "8px",
                  color: "white",
                  cursor: "pointer",
                }}
              />
            </div>
          )}
          <div className="titles-service mt-5">
            <span
              className={
                toggle
                  ? "px-3 py-2 first-about-light"
                  : "px-3 py-2 first-about-dark"
              }
            >
              SERVICES
            </span>
            <h3 style={{ color: toggle ? "#000" : "#fff" }}>What I Do</h3>
          </div>
          {thingsTodo}
          <Modal show={show} onHide={handleClose}>
            <Modal.Header closeButton>
              <Modal.Title>Modal heading</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <Container>
                <Row>
                  <Col>
                    <div style={{ maxWidth: "100%", overflow: "hidden" }}>
                      <Image
                        src="https://cdn.pixabay.com/photo/2023/11/05/17/13/orange-8367924_960_720.jpg"
                        fluid
                        className="mb-2"
                      />
                    </div>
                    <h3>Something Something</h3>
                    <span>
                      Lorem ipsum dolor sit amet consectetur adipisicing elit.
                      Officia nihil eos beatae magnam voluptatibus reiciendis et
                      consequatur. Consequuntur cupiditate error perferendis
                      quidem quibusdam quia quae, nostrum, ad atque vitae unde?
                    </span>
                  </Col>
                </Row>
              </Container>
            </Modal.Body>
          </Modal>
          <Row
            style={{ backgroundColor: toggle ? "#fff" : "#000" }}
            className="py-5 px-3 ssss "
          >
            <h3
              style={{ color: toggle ? "#000" : "#fff" }}
              className="bar-progress-title pb-5 "
            >
              Partners
            </h3>
            {partners}
          </Row>
          <Row className="py-5 first-part">
            <h3
              style={{ color: toggle ? "#000" : "#fff" }}
              className="bar-progress-title pb-5"
            >
              Fun Facts
            </h3>
            {funFact}
          </Row>
          <Row
            style={{ backgroundColor: toggle ? "#fff" : "#000" }}
            className="py-5 first-part"
          >
            <h3
              style={{ color: toggle ? "#000" : "#fff" }}
              className="bar-progress-title pb-4"
            >
              Pricing
            </h3>
            {priceTag}
          </Row>
        </Col>
      </Row>
    </Container>
  );
}

export default Services;
