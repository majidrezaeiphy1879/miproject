import React from "react";
import { useContext } from "react";
import { modeContext } from "../context/modeContext";
import Navbarmenue from "../components/Navbar";
import { Link } from "react-router-dom";
import ToggleButtonMode from "../components/Togglebtn";
import Sidenav from "../components/Sidenav";
import TypingEffect from "../components/Typeanime";
import instaDark from "../images/instagramDark.svg";
import instaLight from "../images/instagramLight.svg";
import gitlabD from "../images/gitlabD.png";
import gitlabL from "../images/gitlabL.png";
import tiktokDark from "../images/tiktokDark.svg";
import tiktokLight from "../images/tiktokLight.svg";
import dribblerDark from "../images/dribbbleDark.svg";
import dribblerLight from "../images/dribbbleLight.svg";
import linkdinDark from "../images/linkedinDark.svg";
import linkdinLight from "../images/linkedinL.svg";
import avatar from "../images/photo_2023-11-09_15-30-43.jpg";
import itelsf from '../images/itself.jpg'
import { Col, Container, Row, Image } from "react-bootstrap";

const words = ["Programmer", "Astrophysicist", "Web Developer"];
function Home() {
  const { toggle, linkItems } = useContext(modeContext);

  linkItems.map((item, index) => (
    <div key={index} className="d-flex align-items-center py-2">
      {toggle ? (
        <>
          <Image
            src={item.imageDark}
            className="me-2"
            style={{ height: "15px", width: "15px" }}
          />
          <Link to={item.path} className="side-nav-link-style">
            {item.name}
          </Link>
        </>
      ) : (
        <>
          <Image
            src={item.imageLight}
            className="me-2"
            style={{ height: "15px", width: "15px", color: "white" }}
          />
          <Link to={item.path} className="side-nav-link-style">
            {item.name}
          </Link>
        </>
      )}
    </div>
  ));

  return (
    <Container fluid>
      <Row className="d-xl-none">
        <Col style={{ paddingRight: "0", paddingLeft: "0" }}>
          <Navbarmenue />
        </Col>
      </Row>
      <Row>
        <Col
          className={
            toggle
              ? "fixed-col-light fixed-col col-3"
              : "fixed-col-dark col-3 fixed-col"
          }
        >
          <Sidenav />
        </Col>
        <Col
          style={{ overflowY: "auto"}}
          className={
            toggle
              ? " relative-col relative-col-light d-flex align-items-center ps-5 justify-content-center home-structure "
              : " relative-col relative-col-dark  d-flex align-items-center ps-5 justify-content-center home-structure"
          }
        >
          <div>
            <img src={avatar} alt={avatar} className="home-image mx-5 mb-1" />
          </div>
          <div className="home-text ">
            {toggle ? (
              <h3 className="mt-3 home-h3-title-light">ADRIANO SMITH</h3>
            ) : (
              <h3 className="mt-3 home-h3-title-dark">ADRIANO SMITH</h3>
            )}
            <TypingEffect words={words} />
            <p className="mt-3 mb-4 home-para ">
              Driven and diligent individual with a alot of interests, from
              frontend coding to mathematics and physics.{" "}
            </p>
            <div className="d-flex mt-3 home-social-media-icons ">
              <a
                target="_blank"
                href="http://www.linkedin.com/in/majid-rezaei-phy"
                className="home-socialm-icon "
              >
                {toggle ? (
                  <img
                    src={linkdinDark}
                    className="me-1 social-media-icons"
                    alt=""
                    style={{ height: "27px", width: "30px" }}
                  />
                ) : (
                  <img
                    src={linkdinLight}
                    className="me-1 social-media-icons"
                    alt=""
                    style={{ height: "27px", width: "30px" }}
                  />
                )}
              </a>
              <a target="_blank" href="#">
                {toggle ? (
                  <img
                    src={gitlabD}
                    alt=""
                    style={{ height: "28px", width: "35px" }}
                    className="px-2 social-media-icons"
                  />
                ) : (
                  <img
                    src={gitlabL}
                    alt=""
                    style={{ height: "27px", width: "35px" }}
                    className="px-2 social-media-icons"
                  />
                )}
              </a>
              <a target="_blank" href="#">
                {toggle ? (
                  <img
                    src={instaDark}
                    alt=""
                    style={{ height: "25px", width: "40px" }}
                    className="px-2 social-media-icons"
                  />
                ) : (
                  <img
                    src={instaLight}
                    alt=""
                    style={{ height: "25px", width: "40px" }}
                    className="px-2 social-media-icons"
                  />
                )}
              </a>
              <a target="_blank" href="#">
                {toggle ? (
                  <img
                    src={dribblerDark}
                    alt=""
                    style={{ height: "20px", width: "35px" }}
                    className="px-2 social-media-icons"
                  />
                ) : (
                  <img
                    src={dribblerLight}
                    alt=""
                    style={{ height: "20px", width: "35px" }}
                    className="px-2 social-media-icons"
                  />
                )}
              </a>
              <a target="_blank" href="#">
                {toggle ? (
                  <img
                    src={tiktokDark}
                    alt=""
                    style={{ height: "22px", width: "35px" }}
                    className="px-2 social-media-icons"
                  />
                ) : (
                  <img
                    src={tiktokLight}
                    alt=""
                    style={{ height: "22px", width: "35px" }}
                    className="px-2 social-media-icons"
                  />
                )}
              </a>
            </div>
          </div>
          <ToggleButtonMode />
        </Col>
      </Row>
    </Container>
  );
}

export default Home;
