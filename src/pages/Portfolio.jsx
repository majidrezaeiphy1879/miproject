import React from "react";
import { Container, Col, Row, Image } from "react-bootstrap";
import { useContext, useState } from "react";
import { modeContext } from "../context/modeContext";
import Sidenav from "../components/Sidenav";
import Navbarmenue from "../components/Navbar";
import { FaMoon } from "react-icons/fa";
import { FaRegSun } from "react-icons/fa6";
const portfolioLinks = [
  {
    id: 1,
    src: "https://tokyo-nextjs.netlify.app/_ipx/w_640,q_75/%2Fimg%2Fportfolio%2F5.jpg?url=%2Fimg%2Fportfolio%2F5.jpg&w=640&q=75",
  },
  {
    id: 2,
    src: "https://tokyo-nextjs.netlify.app/_ipx/w_640,q_75/%2Fimg%2Fportfolio%2F2.jpg?url=%2Fimg%2Fportfolio%2F2.jpg&w=640&q=75",
  },
  {
    id: 3,
    src: "https://tokyo-nextjs.netlify.app/_ipx/w_640,q_75/%2Fimg%2Fportfolio%2F4.jpg?url=%2Fimg%2Fportfolio%2F4.jpg&w=640&q=75",
  },
  {
    id: 4,
    src: "https://tokyo-nextjs.netlify.app/_ipx/w_640,q_75/%2Fimg%2Fportfolio%2F3.jpg?url=%2Fimg%2Fportfolio%2F3.jpg&w=640&q=75",
  },
  {
    id: 5,
    src: "https://tokyo-nextjs.netlify.app/_ipx/w_640,q_75/%2Fimg%2Fportfolio%2F6.jpg?url=%2Fimg%2Fportfolio%2F6.jpg&w=640&q=75",
  },
  {
    id: 6,
    src: "https://tokyo-nextjs.netlify.app/_ipx/w_640,q_75/%2Fimg%2Fportfolio%2F7.jpg?url=%2Fimg%2Fportfolio%2F7.jpg&w=640&q=75",
  },
];

function Portfolio() {
  const [selectedCategory, setSelectedCategory] = useState("All");

  const { toggle, handleClick } = useContext(modeContext);

  const handleClickCategory = (category) => {
    setSelectedCategory(category);
  };

  const filteredItems = portfolioLinks.filter((item, index) => {
    if (selectedCategory === "All") {
      return true;
    } else if (selectedCategory === "Vimeo") {
      return index === 0;
    } else if (selectedCategory === "Youtube") {
      return index === 1;
    } else if (selectedCategory === "Photography") {
      return index === 2 || index === 3;
    } else if (selectedCategory === "Details") {
      return index === 4 || index === 5;
    }
  });

  const itemsMenu = filteredItems.map((item, index) => (
    <div className="image-container">
      <Image className="image-port mb-5" fluid src={item.src} style={{height:'450px'}} />
    </div>
  ));

  const col1Items = itemsMenu.filter((item, index) => index % 2 === 0);
  const col2Items = itemsMenu.filter((item, index) => index % 2 !== 0);
  return (
    <Container fluid className="container-padding">
      <Row className="d-xl-none">
        <Col
          className={toggle ? "px-0 top-navbar-dark" : "px-0 top-navbar-light"}
        >
          <Navbarmenue />
        </Col>
      </Row>
      <Row>
        <Col
          xl={3}
          className={
            toggle
              ? " vh-100 fixed-col fixed-col-light "
              : " vh-100 fixed-col-dark  fixed-col"
          }
          style={{ zIndex: "10", position: "sticky", top: 0 }}
        >
          <Sidenav />
        </Col>
        <Col
          className={
            toggle
              ? "scrollable-column  scroll-col bg-color-dark"
              : "scrollable-column  scroll-col bg-color-light"
          }
          style={{ overflowY: "auto" }}
        >
          {toggle ? (
            <div className="about-toggle-btn-dark " onClick={handleClick}>
              <FaMoon
                style={{
                  height: "35px",
                  width: "25px",
                  marginLeft: "10px",
                  marginTop: "7px",
                  cursor: "pointer",
                }}
              />
            </div>
          ) : (
            <div className="about-toggle-btn-light" onClick={handleClick}>
              <FaRegSun
                style={{
                  height: "30px",
                  width: "25px",
                  marginLeft: "1rem",
                  marginTop: "8px",
                  color: "white",
                  cursor: "pointer",
                }}
              />
            </div>
          )}
          <Row className="first-part">
            <Row className="flex-row-container flex-wrapper">
              <Col md={6} className="portfoli-title-item">
                <div className=" mb-3 mt-5 text-start">
                  <span
                    className={
                      toggle
                        ? "px-3 py-2 first-about-light portfoli-title-item1"
                        : "px-3 py-2 first-about-dark portfoli-title-item1"
                    }
                    style={{ color: toggle ? "#333" : "#999" }}
                  >
                    PORTFOLIO
                  </span>
                  <h3
                    style={{ color: toggle ? "#000" : "#fff" }}
                    className="my-3 portfoli-title"
                  >
                    Creative Portfolio
                  </h3>
                </div>
              </Col>

              <Col md={6} className="portfoli-title-item1  flex-wrapper">
                <ul
                  style={{
                    listStyle: "none",
                    color: toggle ? "#767676" : "#bbb",
                    float: "right",
                  }}
                  className="d-flex p-0 mb-4 pe-2 "
                >
                  <li
                    className={`portfolio-list me-1 ${
                      selectedCategory === "All" ? "active" : ""
                    }`}
                    onClick={() => handleClickCategory("All")}
                  >
                    All
                  </li>
                  <li
                    className={`mx-2 portfolio-list ${
                      selectedCategory === "Vimeo" ? "active" : ""
                    }`}
                    onClick={() => handleClickCategory("Vimeo")}
                  >
                    Vimeo
                  </li>
                  <li
                    className={`mx-2 portfolio-list ${
                      selectedCategory === "Youtube" ? "active" : ""
                    }`}
                    onClick={() => handleClickCategory("Youtube")}
                  >
                    Youtube
                  </li>
                  <li
                    className={`mx-2 portfolio-list ${
                      selectedCategory === "Photography" ? "active" : ""
                    }`}
                    onClick={() => handleClickCategory("Photography")}
                  >
                    Photography
                  </li>
                  <li
                    className={`portfolio-list ms-1 ${
                      selectedCategory === "Details" ? "active" : ""
                    }`}
                    onClick={() => handleClickCategory("Details")}
                  >
                    Details
                  </li>
                </ul>
              </Col>
            </Row>

            <Row className="p-0 m-0 my-5">
              <Col md={6}>{col1Items}</Col>
              <Col md={6}>{col2Items}</Col>
            </Row>
          </Row>
        </Col>
      </Row>
    </Container>
  );
}

export default Portfolio;
