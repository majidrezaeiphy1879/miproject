import React, { useContext, useState } from "react";
import { modeContext } from "../context/modeContext";
import tokyo from "../images/_img_logo_dark.png";
import { useNavigate } from "react-router-dom";
import { Col, Container, Row, Image, Carousel } from "react-bootstrap";

function Entery() {
  const { setToggle, toggle } = useContext(modeContext);
  const history = useNavigate();
  const images = [
    { id: 1, src: "https://tokyo-nextjs.netlify.app/img/intro/light.png" },
    { id: 2, src: "https://tokyo-nextjs.netlify.app/img/intro/dark.png" },
  ];
  const [index, setIndex] = useState(0);
  const [activeButton, setActiveButton] = useState(0);
  const handlePrev = () => {
    setIndex((prevIndex) =>
      prevIndex > 0 ? prevIndex - 1 : images.length - 1
    );
    setActiveButton(0);
  };
  const handleNext = () => {
    setIndex((prevIndex) =>
      prevIndex < images.length - 1 ? prevIndex + 1 : 0
    );
    setActiveButton(1);
  };

  function handleImage(imageId) {
    const currentImageObj = images.find((img) => img.id === imageId);
    switch (currentImageObj.id) {
      case 1:
        setToggle(true);
        history("/Home");
        setIndex(prevIndex=>prevIndex(0))
        console.log(currentImageObj.id);
        break;
      case 2:
        setToggle(false);
        history("/Home");
        setIndex(prevIndex=>prevIndex(0))

        console.log(currentImageObj.id);

        break;
      default:
        break;
    }
  }

  return (
    <Container>
      <Row className="entery-bg">
        <Col>
          <div className="d-flex flex-column justify-content-center align-items-center mt-5 entery-images-responsive">
            <Image src={tokyo} />
            <h3 className="mt-3 text-center">
              Personal Portfolio <strong>React NextJS</strong> Template
            </h3>
          </div>
          <Col xs={12} className="d-sm-none">
            <div className="carousel-div">
              <Carousel
                interval={null}
                controls={null}
                indicators={null}
                activeIndex={index}
                onSelect={() => {}}
                className="mt-5"
              >
                <Carousel.Item className="py-3">
                  <Image
                    src={images[0].src}
                    fluid
                    className="entery-images "
                    onClick={() => {
                      setIndex(0);
                      setActiveButton(0);
                      handleImage(images[0].id);
                    }}
                  />
                </Carousel.Item>
                <Carousel.Item className="py-3">
                  <Image
                    src={images[1].src}
                    fluid
                    className="entery-images "
                    onClick={() => {
                      setIndex(1);
                      setActiveButton(1);
                      handleImage(images[1].id);
                    }}                  />
                </Carousel.Item>
              </Carousel>

              <div className=" d-flex justify-content-center mt-5">
                <div className="carousel-buttons me-2" onClick={handlePrev}>
                  <div
                    className={`carousel-inner-btn${
                      activeButton === 1 ? "active" : ""
                    }`}
                  ></div>
                </div>
                <div className="carousel-buttons" onClick={handleNext}>
                  <div
                    className={`carousel-inner-btn${
                      activeButton === 0 ? "active" : ""
                    }`}
                  ></div>
                </div>
              </div>
            </div>
          </Col>
          <Row className=" d-none d-sm-flex justify-content-center align-items-center">
            <Col sm={6}>
              <Image
                src={images[0].src}
                fluid
                className="entery-images"
                onClick={() => {
                  setIndex(0);
                  handleImage(images[0].id);
                }}
                
              />
            </Col>
            <Col sm={6}>
              <Image
                src={images[1].src}
                fluid
                className="entery-images"
                onClick={() => {
                  setIndex(1);
                  handleImage(images[1].id);
                }}
              />
            </Col>
          </Row>
        </Col>
      </Row>
    </Container>
  );
}

export default Entery;
