import React, { useState, useContext, useMemo, useEffect } from "react";
import {
  Container,
  Row,
  Col,
  Button,
  Offcanvas,
  Image,
} from "react-bootstrap";
import { FaMoon,FaBars } from "react-icons/fa";
import { FaRegSun } from "react-icons/fa6";
import { NavLink } from "react-router-dom";
import { modeContext } from "../context/modeContext";
import { RxCross1 } from "react-icons/rx";
import tokyo from '../images/_img_logo_dark.png'
function Navbarmenue() {
  const [show, setShow] = useState(false);
  const { toggle, linkItems, handleClick } = useContext(modeContext);
  const [isCrossIcon, setCrossIcon] = useState(false);

  const navLinks = useMemo(() => {
    return linkItems.map((item, index) => (

      <div key={index} className="d-flex align-items-center py-2">
        {toggle ? (
          <>
            <Image
              src={item.imageDark}
              className="me-2"
              style={{ height: "15px", width: "15px" }}
            />
            <NavLink to={item.path}  className="text-decoration-none text-dark side-nav-link-dark">
              {item.name}
            </NavLink>
          </>
        ) : (
          <>
            <Image
              src={item.imageLight}
              className="me-2"
              style={{ height: "17px", width: "17px"}}
            />
            <NavLink to={item.path} className="text-decoration-none text-white side-nav-link-light">
              {item.name}
            </NavLink>
          </>
        )}
      </div>
    ));
  }, [toggle, linkItems]);

  useEffect(() => {
    const transitionTimeout = setTimeout(() => {
      setCrossIcon((prevIsCrossIcon) => !prevIsCrossIcon);
    }, 300); 

    return () => {
      clearTimeout(transitionTimeout);
    };
  }, [show]);

  const handleShowToggle = () => {
    setShow((prevShow) => !prevShow);
  };


  return (
    <Container fluid>
      <Row className={toggle ? "py-3 px-2 d-flex align-items-end" : "d-flex align-items-end py-3 bg-dark px-2"}>
        <Col>
          {/* <h3 className={toggle ? "side-nav-brand ms-3" : "side-nav-brand ms-3 text-white"}>T O K Y O</h3> */}
          <Image src={tokyo} className="pb-2" />
        </Col>
        <Col className="d-flex justify-content-end align-items-center">
          {toggle ? (
            <div className="top-toggle-mode-light me-2" onClick={handleClick} style={{ zIndex: show ? 9999 : 'auto' }}>
              <FaMoon style={{ height: '20px', width: '20px', marginLeft: '10px', marginTop: '9px',cursor:'pointer' }} />
            </div>
          ) : (
            <div className="top-toggle-mode-light me-2" onClick={handleClick} style={{ zIndex: show ? 9999 : 'auto' }}>
              <FaRegSun style={{ height: '20px', width: '20px', marginLeft: '10px', marginTop: '9px', color: 'white' ,cursor:'pointer'}} />
            </div>
          )}

         <Button onClick={handleShowToggle} style={{ zIndex: show ? 9999 : "auto", border:'none' }}  className={`icon-button ${isCrossIcon ? "cross" : ""} ${toggle ? "bg-white" : "bg-dark"}`}>
            {isCrossIcon ? (
              <FaBars style={{ height: '20px', width: '20px', color: toggle ? 'black' : 'white' }} className="icon-button"/>
            ) : (
              <RxCross1 style={{ height: '20px', width: '20px', color: toggle ? 'black' : 'white' }} className="icon-button"/>
            )}
          </Button>


          <Offcanvas show={show} onHide={handleShowToggle} id="off-canvas-display">
            <Offcanvas.Body className={toggle?"off-canvas-side-nav-light ps-5":"off-canvas-side-nav-dark ps-5"}>
              <h3 className={toggle?"text-dark mb-3":"text-white mb-3"}>TOKYO</h3>
              {navLinks}
            </Offcanvas.Body>
          </Offcanvas>
          {show && (
            <div
              className="overlay"
              onClick={handleShowToggle}
              style={{ zIndex: 9999 }}
            ></div>
          )}
        </Col>
      </Row>
    </Container>
  );
}

export default Navbarmenue;
