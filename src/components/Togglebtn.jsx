import React, { useState, useContext } from 'react'
import { FaMoon } from 'react-icons/fa';
import { FaRegSun } from "react-icons/fa6";
import { modeContext } from '../context/modeContext';
function TogglebtnMode() {
const {toggle, handleClick}=useContext(modeContext)
  return (
    <div className='toogle-btn-mode' onClick={handleClick}>
    {toggle ? (
      <div className='toogle-btn-dark '>
        <FaMoon style={{height:'30px', width:'25px',marginLeft:'1rem',marginTop:'8px',cursor:'pointer'}} />
      </div>
    ) : (
      <div className='toogle-btn-light '>
        <FaRegSun style={{height:'30px', width:'25px',marginLeft:'1rem',marginTop:'8px',color:'white', cursor:'pointer'}} />
      </div>
    )}
  </div>
  )
}

export default TogglebtnMode