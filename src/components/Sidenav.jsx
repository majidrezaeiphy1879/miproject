import React from "react";
import { NavLink } from "react-router-dom";
import { modeContext } from "../context/modeContext";
import { useContext, useState } from "react";

import { Image } from "react-bootstrap";

function Sidenav() {
  const { toggle, linkItems } = useContext(modeContext);

  const links = linkItems.map((item, index) => (
    <div
      key={index}
      className="d-flex align-items-center py-2 "
      style={{ paddingLeft: "7rem" }}
    >
      {toggle ? (
        <>
          <Image
            src={item.imageDark}
            className="me-2"
            style={{ height: "15px", width: "15px" }}
          />
          <NavLink to={item.path} className="side-nav-link-light  ">
            {item.name}
          </NavLink>
        </>
      ) : (
        <>
          <Image
            src={item.imageLight}
            className="me-2"
            style={{ height: "15px", width: "15px" }}
          />
          <NavLink
            to={item.path}
            className="side-nav-link-dark "
            activeClassName="link-active-button"
          >
            {item.name}
          </NavLink>
        </>
      )}
    </div>
  ));

  return (
    <>
      <h4 className="side-nav-link-title" style={{color:toggle? '#000':'#fff'}}>TOKYO</h4>
        
      
      {links}
      {toggle ? (
        <span className="mt-5 text-dark" style={{ paddingLeft: "7rem" }}>
          &copy; made by MAJID
        </span>
      ) : (
        <span className="mt-5 text-white" style={{ paddingLeft: "7rem" }}>
          &copy; made by MAJID
        </span>
      )}
    </>
  );
}

export default Sidenav;
