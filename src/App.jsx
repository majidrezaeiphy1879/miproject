import { BrowserRouter, Routes, Route } from "react-router-dom";
import "./App.css";
import Home from "./pages/Home";
import Services from "./pages/Services";
import About from "./pages/About";
import { useContext, useState } from "react";
import { modeContext } from "./context/modeContext";
import Homelight from "./images/homeLight.png";
import AboutLight from "./images/aboutLigth.png";
import ServiceLight from "./images/servicelight.svg";
import NewsDark from "./images/newsDark.svg";
import PortfolioDark from "./images/portfolioDark.svg";
import Portfolio from "./pages/Portfolio";
import Entery from "./pages/Entery";
const linkItems = [
  {
    name: "Home",
    path: "/Home",
    imageDark: "https://tokyo-nextjs.netlify.app/img/svg/home-run.svg",
    imageLight: Homelight,
  },
  {
    name: "About",
    path: "/about",
    imageDark: "https://tokyo-nextjs.netlify.app/img/svg/avatar.svg",
    imageLight: AboutLight,
  },
  {
    name: "Services",
    path: "/Services",
    imageDark: "https://tokyo-nextjs.netlify.app/img/svg/briefcase.svg",
    imageLight: PortfolioDark,
  },
  {
    name: "Portfolio",
    path: "/portfolio",
    imageDark: "https://tokyo-nextjs.netlify.app/img/svg/setting.svg",
    imageLight: ServiceLight,
  },
  {
    name: "Contact Me",
    path: "/",
    imageDark: "https://tokyo-nextjs.netlify.app/img/svg/paper.svg",
    imageLight: NewsDark,
  },
];

function App() {
  const [toggle, setToggle] = useState(true);
  function handleClick() {
    setToggle((prevToggle) => !prevToggle);
  }
  return (
    <modeContext.Provider value={{ handleClick, toggle, linkItems ,setToggle}}>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Entery />} />
          <Route path="/Home" element={<Home />} />
          <Route path="/about" element={<About />} />
          <Route path="/portfolio" element={<Portfolio />} />
          <Route path="/services" element={<Services />} />
        </Routes>
      </BrowserRouter>
    </modeContext.Provider>
  );
}

export default App;
